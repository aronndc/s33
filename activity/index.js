console.log("Hello World!");
console.log(" ");


// 3 CODES

async function getTodos() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos");
    let resJson = await response.json();
    console.log(resJson);


// 4 CODES

	let titles = resJson.map((json) => "Title: " + json.title);
    console.log(titles);
}
getTodos();


// 5 CODES

fetch("https://jsonplaceholder.typicode.com/todos/26")
    .then((res) => res.json())
    .then((json) => console.log(json));


// 6 CODES

fetch("https://jsonplaceholder.typicode.com/todos/26")
    .then((res) => res.json())
    .then((json) => {
        console.log(`title: ${json.title} | completed: ${json.completed}`);
    });


// 7 CODES

async function addTodo() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            userId: 1,
            completed: true,
            title: "New Post"
        })
    });

    let resJson = await response.json();
    console.log(resJson);
}
addTodo();


// 8 CODES

async function updateTodo() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos/26", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "This is an updated to-do"
        })
    });

    let resJson = await response.json();
    console.log(resJson);
}
updateTodo();


// 9 CODES

async function updateTodoStructure() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos/26", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "This is a restructured to-do",
            description: "This is a new description",
            status: "Completed",
            date_completed: "08-26-2022",
            userId: 123456
        })
    });

    let resJson = await response.json();
    console.log(resJson);
}
updateTodoStructure()


// 10 CODES

async function patchTodo() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos/26", {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "This is a patched to-do",
        })
    });

    let resJson = await response.json();
    console.log(resJson);
}
patchTodo();


// 11 CODES

async function statusTodo() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos/26", {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            completed: true,
            date_completed: "08-26-2022"
        })
    });

    let resJson = await response.json();
    console.log(resJson);
}
statusTodo();


// 12 CODES

async function deleteTodo() {
    let response = await fetch("https://jsonplaceholder.typicode.com/todos/26", {
        method: "DELETE"
    });

    let resJson = await response.json();
    console.log(resJson);
    console.log("Method Deleted");
}
deleteTodo();